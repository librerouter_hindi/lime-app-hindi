import { h, Fragment } from 'preact';
import {useState} from'preact/hooks';


import { useAppContext } from '../../utils/app.context';
import style from './style';
import { useBoardData } from 'utils/queries';
import i18n, { dynamicActivate } from '../../i18n';
export const Header = ({ Menu }) => {

	
	const { data: boardData } = useBoardData();
	const { menuEnabled } = useAppContext();
	console.log(useState);
	const [ menuOpened, setMenuOpened ] = useState(false);
	var DefaultLan = localStorage.getItem("DefaultAppLanguage");
	console.log("DefaultLan",DefaultLan);
	if(!DefaultLan){
		DefaultLan = 'en';
	}
	const [selectedLangauge,setSelectedLangauge]   = useState(DefaultLan);


	

	function toggleMenu() {
		setMenuOpened(prevValue => !prevValue);
	}

	function langaugedropdownChange(selectedLn){
		if(selectedLn){
			console.log("langaugedropdownChange",selectedLn.target.value);
			localStorage.setItem("DefaultAppLanguage", selectedLn.target.value);
			dynamicActivate(selectedLn.target.value);
			setSelectedLangauge(selectedLn.target.value);
		}
		
	}

	return (
		<Fragment>
			<header class={style.header}>
				{menuEnabled &&
				//<div className={`${style.hamburger} ${menuOpened ? style.isActive : ''}`}
				<div className={`${style.hamburger}`}
					onClick={toggleMenu}
				>
					<span>toogle menu</span>
				</div>
				}
				<h1>{boardData.hostname}</h1>

				<div className='settings-drp'>
				<select onChange={langaugedropdownChange.bind(this)} value={selectedLangauge} id='langauge-dropdown' class="selectpicker" 
					data-style="btn-success">
					<option value={'en'}>English</option>
					<option value={'hi'}>हिन्दी</option>
				</select>
				</div>
				
			</header>
			<Menu opened={menuOpened} toggle={toggleMenu} />
		</Fragment>
		
	);
};
