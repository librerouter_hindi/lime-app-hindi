import { h } from 'preact';
import Router from 'preact-router';
import { ReactQueryCacheProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query-devtools';

import { Provider } from 'react-redux';
import { store } from '../store';
import { history } from '../store/history';

import { plugins } from '../config';

import { AppContextProvider } from '../utils/app.context';
import { Route, CommunityProtectedRoute, Redirect } from '../utils/routes';

import { Menu } from '../containers/Menu';
import { Header } from './header';
import SubHeader from '../containers/SubHeader';

import queryCache from 'utils/queryCache';

import { useSession, useLogin } from 'utils/queries';

import { RebootPage } from '../containers/RebootPage';
import i18n, { dynamicActivate } from '../i18n';
import { I18nProvider } from '@lingui/react'
import { fromNavigator } from "@lingui/detect-locale";

import { useState, useEffect } from'preact/hooks';





const Routes = () => (
	<Router history={history}>
		{/* Public pages, don't need to be authenticated */}
		{plugins
			.filter(plugin => !plugin.isCommunityProtected)
			.map(Component =>
			(<Route path={Component.name.toLowerCase()}>
				<Component.page />
			</Route>))
		}
		{/* Protected pages, need to be authenticated */}
		{plugins
			.filter(plugin => plugin.isCommunityProtected)
			.map(Component =>
			(<CommunityProtectedRoute path={Component.name.toLowerCase()}>
				<Component.page />
			</CommunityProtectedRoute>))
		}
		{/* Additional plugins routes */}
		{plugins
			.filter(plugin => plugin.additionalRoutes)
			.map(plugin => plugin.additionalRoutes)
			.flat()
			.map(([path, Component]) => (
				<Route path={path}>
					<Component />
				</Route>
			))
		}
		{/* Additional plugins protected routes */}
		{plugins
			.filter(plugin => plugin.additionalProtectedRoutes)
			.map(plugin => plugin.additionalProtectedRoutes)
			.flat()
			.map(([path, Component]) => (
				<CommunityProtectedRoute path={path}>
					<Component />
				</CommunityProtectedRoute>
			))
		}
		<CommunityProtectedRoute path={'/reboot'}><RebootPage /></CommunityProtectedRoute>
		<Redirect default path={'/'} to={'rx'} />
	</Router>
);


const App = () => {
	const { data: session } = useSession();
	const [login, {isIdle}] = useLogin();

	const [lan, setlan] = useState('en');
	if (session.username === null && isIdle) {
		login({username: 'lime-app', password: 'generic'});
		return 'Loading...'
	}
	function changeValue(event){
		setlan(event.target.value);
	}
	function setlanguage(){
		localStorage.setItem("DefaultAppLanguage", lan);
		location.reload();
	}
	var DefaultLan = localStorage.getItem("DefaultAppLanguage");
	
	
	console.log("DefaultLan",DefaultLan);
	if(DefaultLan == null || DefaultLan == undefined){
		return (
			
			<div id="language-modal">
				<ReactQueryDevtools />
				<div className="select-lan-screen">
					<div className="center-content">	
						{lan === 'en' ? <h3>Select language</h3> : <h3>भाषा सेट करें</h3>}
						<div className="wrapper">
							<input type="radio" name="select" id="option-1" value="en" checked={lan === "en"} onChange={changeValue} />
							<input type="radio" name="select" id="option-2" value="hi" onChange={changeValue} checked={lan === "hi"} />
								<label for="option-1" className="option option-1">
									<div className="dot"></div>
									<span>English</span>
								</label>
								<label for="option-2" className="option option-2">
									<div className="dot"></div>
									<span>हिन्दी</span>
								</label>
						</div><br></br>
						<button onClick={setlanguage} className="btn btn-primary btn-continue-custom">
							{lan === 'en' ? 'Continue' : 'जारी रखें'}
						</button>
					</div>
					
				</div>
				
			</div>
		)
	}else{
		 dynamicActivate(DefaultLan);
		return (
			<div id="app">
				<ReactQueryDevtools />
				<Header Menu={Menu} />
				<SubHeader />
				<div id="content" >
					<Routes />
				</div>
			</div>
		)
	}
	

	
}

const AppDefault = () => {
	useEffect(() => {
		dynamicActivate(fromNavigator().split('-')[0]);
	}, []);
	return (
		<I18nProvider i18n={i18n} >
			<ReactQueryCacheProvider queryCache={queryCache}>
				<AppContextProvider>
					<Provider store={store}>
						<App />
					</Provider>
				</AppContextProvider>
			</ReactQueryCacheProvider>
		</I18nProvider >
	);
};

export default AppDefault;
